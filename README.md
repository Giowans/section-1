# Sección 1: Empezando.´

Dentro de la primera sección del curso, el autor nos explica un poco de lo que está constituido el framework, una introducción al curso y por ultimo nos muestra como preparar nuestro equipo para empezar a desarrollar con React Native nuestras aplicaciones.

El primer objetivo es analizar su repositorio (o sea, este) en un editor de código para ver un poco de la constitución del proyecto y saber como correrlo en un dispositivo móvil (ya sea físico o emulado) con el cliente de Expo.